<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PiwebapiSetting;
use App\GridSetting;
use App\TagSetting;
use App\Transformer;
use App\Utility;
use App\Http\Controllers\PIWebAPIController;
use App\Http\Controllers\GridAPIController;

class ConfigController extends Controller
{
    public function getPIConfigurations() {
		$piwebapi = PiwebapiSetting::find(1);
		$gridapi = GridSetting::find(1);
    	return response()->json(['piwebapi' => $piwebapi, 'gridapi' => $gridapi]);
    }

    public function checkPIConnection(Request $request) {
    	$piwebapi = new PIWebAPIController();
    	$piwebapi_details = $piwebapi->getRequest($request->url . '/dataservers', $request->username, $request->password);

		// $grid = new GridAPIController();
		// $grid_details = $grid->getRequest();

    	return response()->json($piwebapi_details);
    }

    public function savePIConnection(Request $request) {
    	if($request->id) {
    		$piwebapi_settings = PiwebapiSetting::find($request->id);
    		$piwebapi_settings->url = $request->url;
    		$piwebapi_settings->username = $request->username;
    		$piwebapi_settings->password = $request->password;
    		$piwebapi_settings->pi_server = $request->pi_server;
    		$piwebapi_settings->save();

    		return response()->json($piwebapi_settings);
    	}
    	else {
    		$piwebapi_settings = new PiwebapiSetting();
    		$piwebapi_settings->url = $request->url;
    		$piwebapi_settings->username = $request->username;
    		$piwebapi_settings->password = $request->password;
    		$piwebapi_settings->pi_server = $request->pi_server;
    		$piwebapi_settings->save();

    		return response()->json($piwebapi_settings);
    	}
	} 
	
	public function getTagConfigurations() {
		$transformers = Transformer::orderBy('xfid', 'asc');
		$pi_config = PiwebapiSetting::find(1)->select('id', 'pi_server');
		$utilities = Utility::orderBy('utility_id', 'ASC')->select('id', 'utility_id');
		return response()->json([
			'pi_config' => $pi_config, 
			'transformers' => $transformers->get(), 
			'utilities' => $utilities->get()
			]);
	}

	public function getTagSettings($id, Request $request) {
		$tag_list = TagSetting::where('utility_id', $id);
		return response()->json($tag_list->get());
	}

	public function getTagDetails(Request $request) {
		$pi_config = PiwebapiSetting::find(1);
		$path = '\\\\' . $pi_config->pi_server . '\\' .  $request->tag_name;

		$piwebapi = new PIWebAPIController();
		return $piwebapi->getRequest($pi_config->url . '/points?selectedFields=webid&path=' . $path, $pi_config->username, $pi_config->password);
	}

	public function validateTags(Request $request) {
		$request_params = json_decode($request->getContent());
		$pi_config = PiwebapiSetting::find(1);

		$params = [];
		foreach($request_params->tags as $k => $t) {
			$params[$k + 1] = [
				'Resource' => $pi_config->url . '/points?selectedFields=webid&path=\\\\' . $pi_config->pi_server . '\\' . $t->tag_name,
				'Method' => 'GET',
			];
		}

		$piwebapi = new PIWebAPIController();
		$piwebapi_response = $piwebapi->postRequest($pi_config->url . '/batch', $pi_config->username, $pi_config->password, $params);

		$tag_status = [];
		foreach($piwebapi_response['data'] as $r) {
			if($r->Status == 200) {
				$tag_status[] = [
					'status' => 200,
					'web_id' => $r->Content->WebId
				];
			}
			else {
				$tag_status[] = [
					'status' => $r->Status,
					'web_id' => ''
				];
			}
		}

		return response()->json($tag_status);
	}

	public function saveTagConfigurations($id, Request $request) {
		$params = json_decode($request->getContent());
		
		$response = [];
		$tag_ids = [];
		foreach($params->tags as $t) {
			if(isset($t->id)) {
				$tag_info = TagSetting::find($t->id);
			}
			else {
				$tag_info = new TagSetting();
			}
			$tag_info->utility_id = $id;
			$tag_info->source_param = $t->source_param;
			$tag_info->tag_name = $t->tag_name;
			$tag_info->web_id = $t->web_id;
			$tag_info->save();

			$tag_ids[] = $tag_info->id;

			$response[] = $tag_info;
		}

		$deleted = TagSetting::where('utility_id', $id)->whereNotIn('id', $tag_ids)->delete();
		
		return response()->json($response);
	}

	public function checkGridConnection(Request $request) {
		$grid = new GridAPIController();
		$start_time = date('Y-m-d\TH:i', strtotime('-15 minutes')) . '+0800';
		$end_time = date('Y-m-d\TH:i') . '+0800';

		$url = env('GRID_URL') . '/getXFcsv.jsp?UTILITY='. $request->utility_id .'&XFID=1000&STARTTIME='. $start_time .'&ENDTIME='. $end_time .'&DIRECTION='. $request->direction .'&DATATYPE=ENHANCED&USER='. $request->username .'&PASSWORD=' . $request->password;
		$filename = 'tester';
		return $grid->getRequest($url, $filename);
	}

	public function saveGridConnection(Request $request) {
		if($request->id) {
			$grid_settings = GridSetting::find($request->id);
		}
		else {
			$grid_settings = new GridSetting();
		}

		$grid_settings->utility_id = $request->utility_id;
		$grid_settings->username = $request->username;
		$grid_settings->password = $request->password;
		$grid_settings->direction = $request->direction;
		$grid_settings->save();

		return response()->json($grid_settings);
	}

	public function addTransformer(Request $request) {
		$params = json_decode($request->getContent());
		$transformer = new Transformer;
		$transformer->xfid = $params->xfid;
		$transformer->save();

		return response()->json($transformer);
	}

	public function deleteTranformer($id) {
		$transformer = Transformer::find($id);
		$transformer->delete();

		return response()->json(['status' => 'success']);
	}

	public function addUtility(Request $request) {
		$params = json_decode($request->getContent());
		$utility = new Utility();
		$utility->utility_id = $params->utility_id;
		$utility->save();

		return response()->json($utility);
	}

	public function deleteUtility($id) {
		$utility = Utility::find($id);
		$utility->delete();

		return response()->json(['status' => 'success']);
	}
}
