<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TagSetting;
use App\PiwebapiSetting;

class PIWebAPIController extends Controller
{
    public function getRequest($url, $username, $password) {
		$curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);    
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(  
            'Accept: application/json',  
            'Content-Type: application/json')  
        );  
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);    
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);  
        curl_setopt($curl, CURLOPT_GSSAPI_DELEGATION, CURLGSSAPI_DELEGATION_FLAG);    
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_GSSNEGOTIATE);    
        curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);  
        curl_setopt($curl, CURLOPT_URL, $url);  
        $result = curl_exec($curl); 
        $output = json_decode($result);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $response = [
        	'httpcode' => $httpcode,
        	'data' => $output
        ];

        return $response;
	}

	public function postRequest($url, $username, $password, $data) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(  
            'Accept: application/json',  
            'Content-Type: application/json',
            'Authorization: Basic ' . base64_encode($username . ":" . $password))
        );  
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "post");                                                                     
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_SLASHES));    
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);    
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);  
        curl_setopt($curl, CURLOPT_HEADER, 0);                     //include response header for debug  
        curl_setopt($curl, CURLOPT_GSSAPI_DELEGATION, CURLGSSAPI_DELEGATION_FLAG);    
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_GSSNEGOTIATE);    
        curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);  
        curl_setopt($curl, CURLOPT_URL, $url);
        $result = curl_exec($curl); 
        $output = json_decode($result);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $response = [
        	'httpcode' => $httpcode,
        	'data' => $output
        ];

        return $response;
    }

    protected function getWebIds($tags, $values) {
        $data = [];
        $web_ids = TagSetting::whereIn('source_param', $tags)->select('web_id', 'source_param', 'tag_name')->get();
        $piwebapi_settings = PiwebapiSetting::find(1);
        $tag_list = [];
        foreach($web_ids as $key => $d) {
            $tag_list[$d->source_param] = [
                'Resource' => $piwebapi_settings->url . '/streams/' . $d->web_id . '/recorded',
                'Content' => json_encode($values[$d->source_param]),
                'Method' => 'POST'
            ];
        }
        return $tag_list;
    }

    public function writeData($tags) {
        $piwebapi_settings = PiwebapiSetting::find(1);

        $data = $this->getWebIds($tags->tag_list, $tags->values);
        $response = $this->postRequest($piwebapi_settings->url . '/batch', $piwebapi_settings->username, $piwebapi_settings->password, $data);
        
        return $response;
    }
}
