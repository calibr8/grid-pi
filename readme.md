## About Grid to PI API

Calibr8 Systems Inc. has developed an API that will automatically retrieve the data from GRID2020 API to PI Data Archive throught PI Web API

### Requirements:
- Web Server (IIS).
- PHP >= 5.6
- PI Web API
- MySQL
- [Composer](https://getcomposer.org/)

### Installation (API Server)

```bash 
git clone https://rolly_domingo@bitbucket.org/calibr8/grid-pi.git
cd grid-pi
composer install
```
- Add website in IIS and set the physical path to "grid-pi" directory
- Visit the page (eg. http://localhost:8000/public)
