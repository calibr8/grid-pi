<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePiwebapiSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('piwebapi_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 50);
            $table->string('username', 50);
            $table->string('password', 50);
            $table->string('pi_server', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('piwebapi_settings');
    }
}
