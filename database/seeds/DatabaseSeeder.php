<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('piwebapi_settings')->insert([
            'url' => 'https://win-m0nr4djtaba/piwebapi',
            'username' => 'administrator',
            'password' => 'C8@dm1n',
            'pi_server' => 'win-m0nr4djtaba',
        ]);

        DB::table('grid_settings')->insert([
            'username' => 'calibr8APIuser',
            'password' => 'RvpQ897$hvdE',
            'direction' => 'FORWARD',
            'data_type' => 'ENHANCED',
        ]);

        DB::table('transformers')->insert([
            'xfid' => 1000,
            'description' => 'Transformer 1000',
        ]);

        DB::table('utilities')->insert([
            'utility_id' => 1003,
        ]);
    }
}
