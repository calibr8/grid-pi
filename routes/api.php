<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, PATCH, DELETE');

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/pi-configurations', 'ConfigController@getPIConfigurations');
Route::post('/check-connection', 'ConfigController@checkPIConnection');
Route::post('/save-connection', 'ConfigController@savePIConnection');
Route::post('/save-grid-connection', 'ConfigController@saveGridConnection');
Route::post('/check-grid-connection', 'ConfigController@checkGridConnection');
Route::get('/tag-configurations', 'ConfigController@getTagConfigurations');
Route::get('/tag-configurations/{id}', 'ConfigController@getTagSettings');
Route::get('/piwebapi/get-tag-details', 'ConfigController@getTagDetails');
Route::post('/validate-tags', 'ConfigController@validateTags');
Route::post('/tag-configurations/{id}', 'ConfigController@saveTagConfigurations');
Route::get('/grid/get-data', 'GridAPIController@getData');
Route::post('/backfill', 'GridAPIController@backFill');
Route::get('/grid/load-sample-data', 'GridAPIController@loadSampleData');
Route::post('/tags/create', 'GridAPIController@createTags');

Route::post('/transformers', 'ConfigController@addTransformer');
Route::post('/transformers/{id}', 'ConfigController@deleteTranformer');

Route::post('/utilities', 'ConfigController@addUtility');
Route::post('/utilities/{id}', 'ConfigController@deleteUtility');

Route::get('/write-data', 'GridAPIController@writeDatatoPI');